public class CommandHandler {

    private final String manualText =
        "> Valid commands:\n" +
        "> generate_name          : generate a random name\n" +
        "> permutation n r        : generate permutation of r from n elements\n" +
        "> fib n                  : get the n-th element in fibonacci sequence\n" +
        "> fib_optimized n        : similar to fibonacci, but way faster\n" +
        "> set_queue <FCFS/LCFS>  : change queue operation to either FCFS or LCFS\n" +
        "> help                   : see this command manual\n" +
        "> quit                   : exit"
    ;

    private final String GENERATE_NAME = "generate_name";
    private final String PERMUTATION = "permutation";
    private final String FIB = "fib";
    private final String FIB_OPTIMIZED = "fib_optimized";

    public Command getJob(String originalCommand, String command, String[] data) throws Exception {
        Command job = null;
        switch(command) {
            case GENERATE_NAME:
                job = new CommandGenerateName();
                break;
            case PERMUTATION:
                job = new CommandPermutation();
                break;
            case FIB:
                job = new CommandFibonacci();
                break;
            case FIB_OPTIMIZED:
                job = new CommandOptimizedFibonacci();
                break;
            default:
                return null;
        }
        job.setQuery(originalCommand);
        job.setData(data);
        if (job.isValid()) {
            return job;
        }
        throw new IllegalArgumentException("> Invalid parameter.");
    }

    public String getManual() {
        return this.manualText;
    }

}
