public class CommandFibonacci extends Command {

    private long fib(long n) {
        if (n <= 1) 
            return n;
        return this.fib(n-1) + this.fib(n-2);
    }

    @Override
    public String execute() {
        long inputNum = Long.parseLong(this.data[0]);
        return Long.toString(this.fib(inputNum));
    }

    @Override
    public String getName() {
        return "fib";
    }

    @Override
    public boolean isValid() {
        return data.length == 1;
    }

}
