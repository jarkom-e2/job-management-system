public class CommandPermutation extends Command {

    private long n;
    private long r;
    private long[] permutArray;
    private boolean[] used;
    private String ret= "\n";

    private void generatePermutationHelper(long pos) {
        if (pos == this.r) {
            for (int i = 0; i < r ; i++) {
                if (i > 0) ret += " ";
                ret += permutArray[i];
            }
            ret += "\n";
        } else {
            for (int i = 1 ; i <= this.n ; i++) {
                if (used[i]) continue;
                permutArray[(int)pos] = (long)i;
                used[i] = true;
                generatePermutationHelper(pos+1);
                used[i] = false;
            }
        }
    }

    private void generatePermutation(long n, long r) {
        permutArray = new long[(int)r];
        used = new boolean[(int)n+1];
        generatePermutationHelper(0);
    }

    @Override
    public String execute() {
        generatePermutation(n, r);
        return ret;
    }

    @Override
    public String getName() {
        return "permutation";
    }

    @Override
    public boolean isValid() {
        this.n = Long.parseLong(this.data[0]);
        this.r = Long.parseLong(this.data[1]);
        return (data.length == 2) && (n>=r) && (r>0);
    }

}
