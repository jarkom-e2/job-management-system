import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class ClientHandler implements Runnable {
    public Socket client;
    public JobHandler jobHandler;
    private ExecutorService jobPool;
    public BufferedReader in;
    public PrintWriter out;
    public ArrayList<ClientHandler> clients;
    
    private static String SET_QUEUE = "set_queue";
    private static String HELP = "help";

    private CommandHandler commandHandler;

    public ClientHandler(Socket clientSocket, ArrayList<ClientHandler> clients) throws IOException {
        this.client = clientSocket;
        this.clients = clients;
        this.in = new BufferedReader(new InputStreamReader(client.getInputStream()));
        this.out = new PrintWriter(client.getOutputStream(), true);

        this.jobPool = Executors.newFixedThreadPool(1);
        this.jobHandler = new JobHandler(this);
        this.jobPool.execute(this.jobHandler);
        this.commandHandler = new CommandHandler();
    }

    @Override
    public void run() {
        try {
            this.printManual();

            while (true) {
                try {
                    String query = in.readLine();
                    String[] commandList = query.split(" ");
                    String command = commandList[0];
                    String[] data = Arrays.copyOfRange(commandList, 1, commandList.length);

                    Command job = this.commandHandler.getJob(query, command, data);
                    if (job != null) {
                        this.jobHandler.addJob(job);
                        out.printf("> Job '%s' was added to queue.\n", query);
                    } else if (command.equals(SET_QUEUE)){
                        this.handleSetQueueCommand(commandList);
                    } else if (command.equals(HELP)) {
                        this.printManual();
                    } else {
                        out.println("> Invalid command.");
                    }
                } catch (IllegalArgumentException ex) {
                    out.println(ex.getMessage());
                }
            }
        } catch (IOException e) {
            out.println(e.getStackTrace());
        } catch (Exception e) {
            out.println(e.getMessage());
        } finally {
            out.close();
            try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void handleSetQueueCommand(String[] command) {
        try {
            if (command.length == 2) {
                this.jobHandler.setQueueMethod(command[1]);
                out.printf("> Queue method set to %s.\n", command[1]);
            } else {
                out.println("> Invalid parameter. Please provide either FCFS or LCFS.");
            }
        } catch (IllegalArgumentException e) {
            out.println(e);
        }
    }

    private void printManual() {
        out.println(this.commandHandler.getManual());
    }

}
