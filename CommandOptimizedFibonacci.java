public class CommandOptimizedFibonacci extends Command {

    private long optimizedFib(long n) {
        long[] dpFibo = new long[(int)n+1];
        dpFibo[0] = 0;
        dpFibo[1] = 1;
        for (int i=2 ; i<=n ; i++) {
            dpFibo[i] = dpFibo[i-1] + dpFibo[i-2];
        }

        return dpFibo[(int)n];
    }

    @Override
    public String execute() {
        long inputNum = Long.parseLong(this.data[0]);
        return Long.toString(this.optimizedFib(inputNum));
    }

    @Override
    public String getName() {
        return "fib_optimized";
    }

    @Override
    public boolean isValid() {
        return data.length == 1;
    }

}
