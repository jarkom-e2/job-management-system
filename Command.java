public abstract class Command {

    protected String query;
    protected String[] data;

    public abstract String execute();

    public abstract String getName();

    public abstract boolean isValid();

    public void setQuery(String query) {
        this.query = query;
    }

    public void setData(String[] data) {
        this.data = data;
    }

    public String toString() {
        return this.query;
    }

}
