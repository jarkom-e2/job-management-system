import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class JobHandler implements Runnable {
    private static String FCFS = "FCFS";
    private static String LCFS = "LCFS";
    private static List<String> QUEUE_METHODS = Arrays.asList(
        FCFS,
        LCFS
    );

    private ClientHandler clientHandler;
    private volatile ArrayList<Command> jobs;
    private volatile String queue_method;

    public JobHandler(ClientHandler clientHandler) throws IOException {
        this.clientHandler = clientHandler;
        this.jobs = new ArrayList<>();
        this.queue_method = FCFS;   
    }

    @Override
    public void run() {
        while (true) {
            if (!this.jobs.isEmpty()) {
                Command job = this.jobs.remove(this.getQueueMethodIndex());
                this.execute(job);
                this.clientHandler.out.println(getRemainingJobs());
            }
        }
    }

    public int getQueueMethodIndex() {
        if (this.queue_method.equals(LCFS)) {
            return this.jobs.size()-1;
        }
        return 0;
    }

    private void outToAll(String message) {
        for (ClientHandler aClient : clientHandler.clients) {
            aClient.out.println(message);
        }
    }

    private void execute(Command job) {
        long startTime = Instant.now().toEpochMilli();
        String jobResult = job.execute();
        long endTime = Instant.now().toEpochMilli();
        String ip;
		try {
			ip = InetAddress.getLocalHost().toString();
		} catch (UnknownHostException e) {
            ip = "Unknown worker";
		}

        long timeElapsed = endTime - startTime;
        String output = String.format(
            "> Output: %s\n> Job '%s' finished with execution time: %d ms.\n> Job executed by worker: '%s'",
            jobResult, job, timeElapsed, ip);
        this.clientHandler.out.println(output);
    }

    public void addJob(Command job) {
        this.jobs.add(job);
    }

    public String getRemainingJobs() {
        return "> Remaining jobs in queue: " + this.jobs.toString();
    }

    public void setQueueMethod(String queue_method) throws IllegalArgumentException {
        if (QUEUE_METHODS.contains(queue_method)) {
            this.queue_method = queue_method;
        } else {
            throw new IllegalArgumentException("> Invalid queue method.");
        }
    }

}
