import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    private static final int SERVER_PORT = 9090;

    public static void main(String[] args) throws IOException {
        String SERVER_IP = args.length == 1 ? args[0] : "localhost";
        Socket socket = new Socket(SERVER_IP, SERVER_PORT);

        ServerConnection serverConn = new ServerConnection(socket);

        (new Thread(serverConn)).start();

        BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter out = new PrintWriter(socket.getOutputStream(), true);

        while (true) {
            String command = keyboard.readLine();

            if (command.equals("quit")) break;

            out.println(command);
        }

        socket.close();
        System.exit(0);
    }
}
