# Job Management System

Job Management System is a Java-based program that uses **Master-Worker** paradigm, where one Master node and several
(minimum of one) Worker node can communicate to each other through socket. This program uses socket programming with a **Client-Server** architecture. Master node will serve as the client that requests a job, and the Worker node, as the server,
will respond to the request by executing and displaying the output of the job.

## List of Commands

|No|Command|Desc|
|---|---|---|
|1|`generate_name`| Generate a random K-POP artist name. |
|2|`permutation n r`| Generate permutation of `r` from `n` elements. | 
|3|`fib n`| Get the `n`-th element in fibonacci sequence. |
|4|`fib_optimized n`| Similar to `fib` in number 3, but with optimized performance. |
|5|`set_queue <FCFS/LCFS>`| Change job queue operation to either `FCFS` or `LCFS` (First/Last Come First Serve). |
|6|`help`| See manual containing the list of available commands. |
|7|`quit`| Exit master node. |

## Installation
* Install Java SDK (of any version).
* Clone the repository.
* Compile with `javac *.java` (optional if code directly executed from IDE).
* Run with `java <class name>`. In this case, you need to run `java Server` first, followed by `java Client`.
* Test any command in the running client terminal and see worker node in action!

If you wish to connect to virtual machine (EC2),
* Install Git and Java SDK in EC2.
* Clone the repository to AWS EC2 instance as the server (worker node).
* Compile with `javac *.java` and run `java Server`.
* If master node (or your PC), run `java Client <insert EC2 public IP>` to connect to EC2 worker node.
* Once connected, see EC2 worker node in action!

## Authors [E2]:
* [Glenda Emanuella Sutanto](https://gitlab.com/glendaesutanto)
* [Kefas Satrio Bangkit Solideantyo](https://gitlab.com/kefassatrio)
* [Wulan Mantiri](https://gitlab.com/wulanmantiri)

## Acknowledgements
* CS UI - Computer Networks C 2020
