public class CommandGenerateName extends Command {

    private static String[] names = {
        "Ryujin", "Sana", "Tzuyu", "Dahyun", "Yeji", 
        "Yuna", "Lia", "Chaeryeong", "Jihyo", "IU", "Min Yoongi", 
        "RM", "JHope", "Taehyung", "Jungkook", "Jin", "Jimin"
    };

    @Override
    public String execute() {
        return names[(int) (Math.random() * names.length)];
    }

    @Override
    public String getName() {
        return "generate_name";
    }

    @Override
    public boolean isValid() {
        return true;
    }

}
